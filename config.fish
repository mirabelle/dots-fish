abbr -a grep "grep --color=auto"
abbr -a o "xdg-open" 
abbr -a e "nvim"  
abbr -a g "git" 
abbr -a m "mailsync ; neomutt" 
abbr -a mkdir "mkdir -pv" 
abbr -a mv 'mv -i'
abbr -a dots "git --git-dir=$HOME/.dots.git --work-tree=$HOME" 
abbr -a docker-clean "docker system prune -af" 
abbr -a l "exa -bF" 
abbr -a ll "exa -abF" 
abbr -a lll "exa -labF --git" 

# Shortcuts
abbr -a ~w "cd ~/Workspace" 
abbr -a ~c "cd ~/.config" 
abbr -a ~g "git rev-parse --is-inside-work-tree 2> /dev/null; and cd (git rev-parse --show-toplevel)"

