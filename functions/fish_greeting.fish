function fish_greeting

printf \n\
"OS: "(lsb_release -sd | sed 's/\"//g')\n\
"User: "(whoami)\n\
"Uptime: "(awk '{printf "%dd %dh %dm", $1/60/60/24, $1/60/60%24, $1/60%60}' /proc/uptime)\n\
"Load Avarage: "(awk '{print $1, $2, $3}' /proc/loadavg)\n\
"Disk Usage: "(df -lh | grep '/root' | awk '{printf "%s / %s", $3, $2}')\n\
"Task: "(task rc.verbose: rc.report.next.columns:description rc.report.next.labels:1 limit:1 next)\n\n

end
